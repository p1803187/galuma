# GALUMA
**===================================================**

**- Projet de L3 (2eme semestre) - Gestion d'un Potager**

    - Gabriel JUSTIN 11920022
    - Lucas BARTHELEMY 11803187
    - Marine MASINGARBE 11501149

**===================================================**


Identifiant du projet : **17729**.


**- Commandes de Compilation :**

Pour compiler, il faut utiliser la commande `javac *.java` dans le dossier _src_, puis `java Main` afin d'executer.


**- Objectif du Projet :**

Développer une application qui optimise les Synergies entre Légumes pour un Potager.


**- Utilisation de l'Application :**

Une fois l'application ouverte, en cliquant sur le menu, vous pouvez acceder aux informations des plantes et la description sur les climats pour mieux comprendre les filtres. Pour l'information des plantes, il suffit de cliquer sur un légume et cela fera apparaitre sa description. 
Sur l'accueil, vous devez choisir la taille de votre terrain puis les plantes que vous souhaitez utiliser. Vous pouvez filtrer vos plantes via les filtres en fonction de la saison de plantation, du climat de votre région, et/ou du groupe de la plante (bulbe, tiges, fleurs etc..). Vous devez obligatoirement sélectionner au moins une plante. Une fois la sélection effectuée, appuyez sur "Valider".

Vous arrivez maintenant sur la page de sélection des parcelles, qu'il faudra remplir avec des entiers, chaque entier désignant une parcelle.
- Par exemple :
- 1|1|2|2
- 1|2|2|3
- 4|4|3|3
- Ici on aura un même légume en 1, un autre en 2, et de même en 3 et en 4.

Une fois cela fait, appuyez sur "Valider". La solution s'affiche, le nombre de solutions et le score sont affichés en haut de la fenêtre. Le bouton "Sauvegarder" permet d'enregistrer la solution en fichier csv, que l'on pourra ensuite importer en format excel. Vous pouvez regarder toute les solutions grâce au bouton "Suivant", "Précédent" ou via le bouton "Aléatoire" (ce dernier est utile si vous avez beaucoup de solutions, car dans ce cas, deux solutions proches se ressemblent énormément).
Vous pouvez changer un légume, en cliquant dessus, puis en sélectionnant la plante de remplacement. Si ce nouveau choix ne vous convient pas, pas de panique, vous pouvez annuler le changement grâce au bouton "Annuler Changement".


**- Description de l'organisation de notre archive :**

- A la racine de notre archive, on retrouve plusieurs répertoires : _java_ et _prolog_ et ce fichier _readme.md_.
- Dans le répertoire _java_ se trouvent les dossiers _src_ et _ressources_.
- Dans le répertoire _prolog_ se trouvent les fichiers prolog.
- Dans le répertoire _src_ se trouvent tous les fichiers des classes java et le Main, l'exécutable, le dossier _util_ et les scripts Bash.
- Dans le répertoire _util_ se trouvent deux classes java utilitaires.
- Dans le répertoire _ressources_ se trouve l'image _synergies_ et un dossier _images_ contenant toutes les icones utilisées dans l'application.
