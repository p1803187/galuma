import util.IntPair;
import util.IntPairList;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;
/*
La classe Potager, servant à faire le lien entre Java et Prolog et faisant certains traitements sur le tableau représentant le potager
*/
public class Potager {

    private final int nbParcelles;
    private final Parcelle[] tabParcelles;

    // constructeur par defaut
    public Potager() {
        nbParcelles = 0;
        tabParcelles = new Parcelle[0];
    }

    // constructeur avec nombre de Parcelles
    public Potager(final int NOMBRE_PARCELLES) {
        nbParcelles = NOMBRE_PARCELLES;
        tabParcelles = new Parcelle[nbParcelles];
    }

    // constructeur par copie
    public Potager(final Potager POTAGER) {
        nbParcelles = POTAGER.nbParcelles;
        tabParcelles = POTAGER.tabParcelles.clone();
    }

    // écrit le fichier prox.pl avec les prédicats "prox(X,Y), synergie(Legume1, Legume2, Score) et legume(Legume)"
    public static void sauvegarderTabProx(final int[][] TAB, final int NB_PARCELLES, Plant[] selecPlantes) {
        IntPair[] tabAdjacences = tabAdjacences(TAB, NB_PARCELLES);//on récupère le tableau d'adjacences pour l'écriture des prox
        String contenu = "";
        for (IntPair tabAdjacence : tabAdjacences) {//écriture des prox(X,Y)
            contenu += "prox(" + tabAdjacence.getLeft() + "," + tabAdjacence.getRight() + ").\n";
        }

        for(int i=0;i<selecPlantes.length;++i)//écriture des legume(Legume)
            contenu +="legume("+selecPlantes[i].getNom().toLowerCase()+").\n";
        contenu+="synergie(X,X,0).\n";
        for(int i=0;i<selecPlantes.length;++i)//écriture des synergie(Legume1, Legume2, Score)
        {

            for(int j=i+1;j< selecPlantes.length;++j)
            {
                contenu +="synergie("+selecPlantes[i].getNom().toLowerCase()+","+selecPlantes[j].getNom().toLowerCase()+","+Plant.getScore(selecPlantes[i],selecPlantes[j])+ ").\n";
                contenu +="synergie("+selecPlantes[j].getNom().toLowerCase()+","+selecPlantes[i].getNom().toLowerCase()+","+Plant.getScore(selecPlantes[j],selecPlantes[i])+ ").\n";
            }
        }
        File prologAdj = new File("../../prolog/prox.pl");//on supprime l'ancien fichier prox.pl et on en crée un nouveau
        if (prologAdj.delete())
            System.out.println("Ancien fichier supprimé");
        try {
            prologAdj.createNewFile();
            System.out.println("Nouveau fichier créé");
        }
        catch (IOException exc) {
            System.out.println("Erreur lors de la création du fichier");
            exc.printStackTrace();
        }
        try {
            FileWriter myWriter = new FileWriter("../../prolog/prox.pl");//écriture de contenu dans le fichier
            myWriter.write(contenu);
            myWriter.close();
            System.out.println("Valeur écrite, succès");
        } catch (IOException e) {
            System.out.println("Erreur lors de l'écriture");
            e.printStackTrace();
        }
    }

    //version alternative de sauvegarderTabProx écrivant les fichiers prolog pour la modification de legumes
    public static void sauvegarderModif(Plant[] legModif,Plant[] legBase)
    {
        boolean newLegSelec;
        String contenu="";//contenu de prox2.pl
        String contenu2="";//contenu de prox.pl
        for(int i=0;i<legModif.length;++i)
        {
            contenu +="legume2("+legModif[i].getNom().toLowerCase()+").\n";//on crée le prédicat legume2 pour la modification des légumes
            newLegSelec=true;
            for(int j=0;j<legBase.length;++j)
            {
                if(legModif[i].getIndex()==legBase[j].getIndex())
                    newLegSelec=false;
            }
            if(newLegSelec)
            {
                for(int j=0;j<legBase.length;++j)//on ajoute les nouvelles synergies à prox.pl
                {
                    contenu2 +="synergie("+legModif[i].getNom().toLowerCase()+","+legBase[j].getNom().toLowerCase()+","+Plant.getScore(legModif[i],legBase[j])+ ").\n";
                    contenu2 +="synergie("+legBase[j].getNom().toLowerCase()+","+legModif[i].getNom().toLowerCase()+","+Plant.getScore(legBase[j],legModif[i])+ ").\n";
                }
            }
        }
        File prologAdj = new File("../../prolog/prox2.pl");
        if (prologAdj.delete())
            System.out.println("Ancien fichier supprimé");
        try {
            prologAdj.createNewFile();
            System.out.println("Nouveau fichier créé");
        }
        catch (IOException exc) {
            System.out.println("Erreur lors de la création du fichier");
            exc.printStackTrace();
        }
        try {
            FileWriter myWriter = new FileWriter("../../prolog/prox2.pl");
            FileWriter myWriter2 = new FileWriter("../../prolog/prox.pl",true);/*on ajoute simplement à la fin du fichier les nouvelles synergies, d'où
                                                                                 le append fixé à true*/
            myWriter.write(contenu);
            myWriter2.append(contenu2);
            myWriter2.close();
            myWriter.close();
            System.out.println("Valeur modif écrite, succès");
        } catch (IOException e) {
            System.out.println("Erreur lors de l'écriture");
            e.printStackTrace();
        }
    }
    // cree le tab d'adjacences a partir du tableau de parcelles récupéré en entrée
    private static IntPair[] tabAdjacences(final int[][] TAB, final int NB_PARCELLES) {
        IntPairList liste = new IntPairList();
        final int X_LENGTH = TAB.length, Y_LENGTH = TAB[0].length;
        for (int x = 0; x < X_LENGTH; x++) {
            for (int y = 0; y < Y_LENGTH; y++) {
                if (x != 0)
                    testeEtRemplie(TAB[x][y], TAB[x-1][y], liste);
                if (x != X_LENGTH-1)
                    testeEtRemplie(TAB[x][y], TAB[x+1][y], liste);
                if (y != 0)
                    testeEtRemplie(TAB[x][y], TAB[x][y-1], liste);
                if (y != Y_LENGTH-1)
                    testeEtRemplie(TAB[x][y], TAB[x][y+1], liste);
            }
        }
        IntPair[] resultat = new IntPair[liste.getLength()];
        for (int i = 0; i < liste.getLength(); i++) {
            resultat[i] = liste.getValue(i);
        }
        return resultat;
    }

    // ajoute une nouvelle adjacence si elle n'est pas déjà présente dans la liste d'adjacences
    private static void testeEtRemplie(final int I1, final int I2, IntPairList liste) {
        if (I1 != I2) {
            IntPair paire = new IntPair(I1, I2);
            if (!liste.contains(paire))
                liste.add(paire);
        }
    }
    //un main qui était précédemment utilisé pour les tests
    public static void main (String[] args) {
        final int[][] tabParcelles={{1,1,2,3},{1,1,2,4},{5,6,7,8}};
        IntPair[] tabAdj=tabAdjacences(tabParcelles,8);
        /* Test avec [[a;a;b;c]
                      [a;a;b;d]
                      [e;f;g;h]]
                      (1,2),(1,5),(1,6),(2,3),(2,4),(2,7),(3,4),(4,8),(5,6),(6,7),(7,8)*/
        for (IntPair intPair : tabAdj) {
            System.out.print(intPair.getLeft() + " ");
            System.out.println(intPair.getRight());
        }
    }
    //une fonction de débug écrivant les relations d'adjacences obtenues par la méthode tabAdjacences dans la console
    public static void printAdj(final int[][] TAB, final int NB_PARCELLES)
    {
        IntPair[] tabAdj=tabAdjacences(TAB,NB_PARCELLES);
        for (IntPair intPair : tabAdj) {
            System.out.print(intPair.getLeft() + " ");
            System.out.println(intPair.getRight());
        }
    }

    public int getNbParcelles() { return nbParcelles; }

}
