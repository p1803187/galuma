import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import static java.lang.System.exit;


public class InterfaceGraphique {

    private final JFrame frame = new JFrame("GALUMA"); // fenetre de l'application
    private final int DIMX = 800, DIMY = 600; // dimensions de la fenetre
    private Plant[] plantsSelec; // plants sélectionnés par l'utilisateur dans le premier menu
    private String[][] tabSolutionTmp; // mise en mémoire des solutions calculées par le prolog

    // contructeur appelé dans le Main.java
    public InterfaceGraphique() {
        frame.setSize(DIMX, DIMY);
        frame.setResizable(false); // empeche la frame d'etre redimensionner manuellement
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE); // stop le programme a la fermeture de la frame
        frame.setLayout(null);
        frame.setLocation(  (Toolkit.getDefaultToolkit().getScreenSize().width - DIMX) / 2, // centre la frame
                (Toolkit.getDefaultToolkit().getScreenSize().height - DIMY) /2  );

        JMenuBar menuBar = new JMenuBar(); // menu en haut à gauche
        JMenu menu = new JMenu("Menu");
        menuBar.add(menu);
        JMenuItem accueil = new JMenuItem("Accueil");
        accueil.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                clearFrame();
                initialisationInterface();
            }
        });
        menu.add(accueil);
        JMenuItem info_leg = new JMenuItem("Infos Plants");
        info_leg.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                clearFrame();
                infoLegumes();//appel l'interface info legume
            }
        });
        menu.add(info_leg);
        JMenuItem climat = new JMenuItem("Définition Climats");
        climat.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                clearFrame();
                descriptionClimat(); // appel l'interface definition climat
            }
        });
        menu.add(climat);
        frame.setJMenuBar(menuBar);

        // permet d'afficher la popup de confirmation à la fermeture de la fenetre via la croix
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                (new BouttonQuitterPopUp(frame)).setVisible(true);
            }
        });
    }


    //interface infos legumes
    public void infoLegumes(){
        clearFrame();
        System.out.println("infoLeg)");

        JLabel l1, l2;
        l1 = new JLabel(" Informations sur les Plants ");
        l1.setBounds(300,10, 600,30);
        l2 = new JLabel(" Cliquez sur le plant pour obtenir des informations sur sa plantation");
        l2.setBounds(180,60, 600,30);
        frame.add(l1);
        frame.add(l2);
        Plant p;

        final int OX=100, OY=90, DX=290, DY=390, IX=15, B=5; // Origine, Dimensions, Interval et Bordure des Panels
        // legumes
        final JPanel panel1 = new JPanel();
        panel1.setBounds(OX, OY, DX+B, DY+B);
        panel1.setBackground(new Color(200, 200, 200));
        // herbes
        final JPanel panel2 = new JPanel();
        panel2.setBounds(OX+DX+IX, OY, DX+B, DY+B);
        panel2.setBackground(new Color(200, 200, 200));

        // affectation des boutons
        for(int i=0; i < Plant.nbPlants(); i++){
            p = Plant.values()[i];
            JButton boutonLegume = new JButton(p.getNom());
            if (p.getIndex() <= 28) {
                panel1.add(boutonLegume);
            } else {
                panel2.add(boutonLegume);
            }
            boutonLegume.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    Plant p;
                    for(int i=0; i<Plant.nbPlants();i++){
                        p = Plant.values()[i];
                        if(p.getNom().equals(boutonLegume.getText())){
                            //acces à la description du légume de la classe plant
                            JOptionPane.showMessageDialog(
                                    frame, p.getDescription(), p.getNom(), JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                }
            });
        }
        frame.add(panel1);
        frame.add(panel2);

        // bouton Retour
        final JButton buttonR = boutonRetour();
        buttonR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                initialisationInterface();
            }
        });
        frame.add(buttonR);

        frame.setVisible(true);
    }

    //interface definition climat
    public void descriptionClimat(){
        clearFrame();
        System.out.println("descripClimat)");
        JLabel l1, l2, l3, l4, l5, l8, l9, l10;
        l1 = new JLabel(" Description Climats");
        l1.setBounds(30,70, 170,20);
        frame.add(l1);
        l2 = new JLabel(" OCÉANIQUE ");
        l2.setBounds(70,100, 170,40);
        frame.add(l2);
        l3 = new JLabel(" Température douce et pluviométrie abondante surtout en octobre-fevrier ");
        l3.setBounds(150,120, 600,50);
        l4 = new JLabel(" CONTINENTAL ");
        l4.setBounds(70,150, 170,50);
        l5 = new JLabel(" Été chaud et hivers rudes avec jour de pluis plus important en été ");
        l5.setBounds(150,170, 600,60);
        l8 = new JLabel(" MEDITERRANEEN ");
        l8.setBounds(70,200, 170,60);
        l9 = new JLabel(" Hivers doux et secs, été chaud et secs, ensoleillement important et vents ");
        l9.setBounds(150,220, 600,80);
        l10 = new JLabel(" violents fréquents. Peu de jours de pluies mais souvent d'orage ");
        l10.setBounds(150,250, 600,70);
        frame.add(l3);
        frame.add(l4);
        frame.add(l5);
        frame.add(l8);
        frame.add(l9);
        frame.add(l10);

        // bouton Retour
        final JButton buttonR = boutonRetour();
        buttonR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                initialisationInterface();
            }
        });
        frame.add(buttonR);
        frame.setVisible(true);

    }

    //interface accueil
    public void initialisationInterface() {
        clearFrame();
        System.out.println("1) Choix des Tailles et Plants");


        //==== PARTIE TAILLES ====//

        JLabel l1, l2, l3;
        l3 = new JLabel("Choix de la taille :");
        l3.setFont(new Font("Arial", Font.BOLD, 16));
        l3.setBounds(50,10, 350,30);
        frame.add(l3);
        l1 = new JLabel("Largeur du terrain : ");
        l1.setFont(new Font("Arial", Font.PLAIN, 14));
        l1.setBounds(50,70, 170,30);
        frame.add(l1);
        l2 = new JLabel("Hauteur du terrain : ");
        l2.setFont(new Font("Arial", Font.PLAIN, 14));
        l2.setBounds(50,170, 170,30);
        frame.add(l2);

        JSpinner spinnerLarg = new JSpinner( new SpinnerNumberModel(
                2, //valeur par defaut
                2, //minimum
                12, //maximum
                1 //step
        ));
        spinnerLarg.setBounds(200,75, 60,20);
        frame.add(spinnerLarg);

        JSpinner spinnerHaut = new JSpinner(new SpinnerNumberModel(
                1, //valeur par defaut
                1, //minimum
                7, //maximum
                1 //step
        ));
        spinnerHaut.setBounds(200, 175, 60, 20);
        frame.add(spinnerHaut);



        //==== PARTIE FILTRES ====//

        final int OX=320, OY=130, DX=220, DY=365, IX=15, B=5;  // Origine, Dimensions, Interval et Bordure des Panels

        // label d'explication
        final JLabel labelPlants = new JLabel("Choisissez vos Plants :");
        labelPlants.setFont(new Font("Arial", Font.BOLD, 16));
        labelPlants.setBounds( OX-B, 10, 500, 30);
        frame.add(labelPlants);

        //filtre saison
        final JLabel labelSaison = new JLabel("Filtrer par Saison : ");
        labelSaison.setBounds(OX+20, 40,300,20);
        labelSaison.setFont(new Font("Arial", Font.PLAIN, 14));
        frame.add(labelSaison);
        final String saisons[] = new String[] {"Aucune","HIVER","PRINTEMPS","ETE","AUTOMNE"};
        final JComboBox<String> cbS = new JComboBox<>(saisons);
        cbS.setBounds(OX+170, 40,150,20);
        frame.add(cbS);
        //filtre climat
        final JLabel labelClimat = new JLabel("Filtrer par Climat : ");
        labelClimat.setBounds(OX+20, 65,300,20);
        labelClimat.setFont(new Font("Arial", Font.PLAIN, 14));
        frame.add(labelClimat);
        final String climats[] = new String[] {"Aucun","OCEANIQUE","CONTINENTAL","MEDITERANEEN"};
        final JComboBox<String> cbC = new JComboBox<>(climats);
        cbC.setBounds(OX+170, 65,150,20);
        frame.add(cbC);
        //filtre groupe
        final JLabel labelGroupe = new JLabel("Filtrer par Groupe : ");
        labelGroupe.setBounds(OX+20, 90,300,20);
        labelGroupe.setFont(new Font("Arial", Font.PLAIN, 14));
        frame.add(labelGroupe);
        final String groupes[] = new String[] {"Aucun","BULBE","TIGES","FRUITS","RACINES","FEUILLES","FLEURS","GRAINES","TUBERCULES","AROMATES", "MEDICINALES"};
        final JComboBox<String> cbG = new JComboBox<>(groupes);
        cbG.setBounds(OX+170, 90,150,20);
        frame.add(cbG);



        //==== PARTIE PLANTS ====//

        // legumes
        JPanel panel1 = new JPanel();
        panel1.setBounds(OX-B, OY-B, DX+B, DY+B);
        panel1.setBackground(GRIS_CLAIR);
        frame.add(panel1);
        // herbes
        JPanel panel2 = new JPanel();
        panel2.setBounds(OX-B+DX+IX, OY-B, DX+B, DY+B);
        panel2.setBackground(GRIS_CLAIR);
        frame.add(panel2);

        // affichage des checkbox
        JCheckBox[] tabcb = new JCheckBox[Plant.nbPlants()];
        affichage_checkbox(cbS, cbC, cbG, panel1, panel2, tabcb);

        // bouton d'application des filtres
        final JButton bOkFiltre = bouton("FILTRER", JAUNE);
        bOkFiltre.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                affichage_checkbox(cbS, cbC, cbG, panel1, panel2, tabcb);
            }
        });
        bOkFiltre.setBounds(OX+340, 80,100,30);
        frame.add(bOkFiltre);

        //bouton de reinitialisation des filtres
        final JButton bReinit = bouton("ANNULER", GRIS_FONCE);
        bReinit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                reinitialiserChoix(cbS, cbC, cbG, tabcb);
                affichage_checkbox(cbS, cbC, cbG, panel1, panel2, tabcb);
            }
        });
        bReinit.setBounds(OX+340, 40,100,30);
        frame.add(bReinit);

        // bouton pour quitter l'application
        final JButton bQuitter = boutonRetour();
        bQuitter.setText("QUITTER");
        bQuitter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                (new BouttonQuitterPopUp(frame)).setVisible(true);
            }
        });
        frame.add(bQuitter);

        // bouton pour valider la saisie et passer au menu suivant
        JButton bValider = boutonValider();
        bValider.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                int hauteur = Integer.parseInt(spinnerHaut.getValue().toString());
                int largeur = Integer.parseInt(spinnerLarg.getValue().toString());
                if (plantsUtiles(tabcb)) { // si au moins un plant selectionne
                    clearFrame();
                    choixParcelles(hauteur, largeur); // go au deuxieme menu (dimensionnement des parcelles)
                } else { // si aucun plant selectionnes
                    JOptionPane.showMessageDialog(frame,"Veuillez sélectionner au moins un Plant pour continuer");
                }
            }
        });
        frame.add(bValider);

        frame.setVisible(true);
    }

    //fonction d'affichage pour les checkbox avec les filtres
    private void affichage_checkbox(JComboBox _cbS, JComboBox _cbC, JComboBox _cbG, JPanel _p1, JPanel _p2, JCheckBox[] _tabcb) {
        final ArrayList<JCheckBox> tabcbs1 = new ArrayList<>();
        final ArrayList<JCheckBox> tabcbs2 = new ArrayList<>();
        Plant p;
        for (int i = 0; i < Plant.nbPlants(); i++) {
            p = Plant.values()[i];
            // pour chaque filtre : si pas de filtre sélectionné ou les données du Plant correspondent au filtre.
            if( (( _cbS.getItemAt(_cbS.getSelectedIndex()) == "Aucune") || (_cbS.getItemAt(_cbS.getSelectedIndex())).equals(p.getSaison().toString()))
                    && (( _cbC.getItemAt(_cbC.getSelectedIndex()) == "Aucun") || (_cbC.getItemAt(_cbC.getSelectedIndex())).equals(p.getClimat().toString()))
                    && (( _cbG.getItemAt(_cbG.getSelectedIndex()) == "Aucun") || (_cbG.getItemAt(_cbG.getSelectedIndex())).equals(p.getGroupe().toString())) ) {
                if (p.getIndex() < Plant.ABSINTHE.getIndex()) { // legume
                    _tabcb[i] = new JCheckBox(p.getNom());
                    tabcbs1.add(_tabcb[i]);
                } else { // herbe
                    _tabcb[i] = new JCheckBox(p.getNom());
                    tabcbs2.add(_tabcb[i]);
                }
                _tabcb[i].setBackground(GRIS_CLAIR);
            }
        }
        _p1.removeAll(); // changer le panel avec le nouvel affichage
        _p2.removeAll();
        frame.revalidate();
        frame.repaint();

        // affichage des plants en deux colonnes dans chaque Panel
        final int   DX_L = (tabcbs1.size()%2 == 0) ?  tabcbs1.size()/2 :  tabcbs1.size()/2 + 1,
                DX_P = (tabcbs2.size()%2 == 0) ?  tabcbs2.size()/2 :  tabcbs2.size()/2 + 1;
        _p1.setLayout( new GridLayout(DX_L, 2) );
        for (final JCheckBox cb: tabcbs1)
            _p1.add(cb);
        _p2.setLayout( new GridLayout(DX_P, 2) );
        for (final JCheckBox cb: tabcbs2)
            _p2.add(cb);

        frame.validate();
        frame.revalidate();
        frame.repaint();
    }

    //annuler les filtres et les choix
    private void reinitialiserChoix(JComboBox _cbS, JComboBox _cbC, JComboBox _cbP, JCheckBox[] _tabcb) {
        _cbS.setSelectedIndex(0); // replacement des filtre sur le premier choix : "Aucun"
        _cbC.setSelectedIndex(0);
        _cbP.setSelectedIndex(0);
        for (JCheckBox cb: _tabcb) // décochage des checkboxes
            cb.setSelected(false);
    }

    // recupérer la liste des plants sélectionnés
    /**
     * Retourne FALSE si aucun Plant n'est sélectionné.
     */
    private boolean plantsUtiles(final JCheckBox[] _tabcb) {
        int nbs = 0; // nb plants selectionnes
        for (JCheckBox cb: _tabcb) { // on compte le nombre de plant sélectionnés...
            if (cb.isSelected())
                nbs++;
        }
        if (nbs == 0) // ... si 0 alors on retourne false (pas de plants sélectionnés)...
            return false;
        Plant[] resultats = new Plant[nbs]; // sinon on crée un tableau de Plant de la taille calculée avant
        int n = 0;
        for (int i = 0; i < nbs; i++) {
            while ((n < _tabcb.length) && (!_tabcb[n].isSelected())) // parcours des checkboxes jusqu'à en trouver
                n++;                                                //  une sélectionnée.
            if (n < _tabcb.length) {
                resultats[i] = Plant.values()[n]; // ajout de ce plant à la liste des plants sélectionnés.
                ++n;
            }
        }
        plantsSelec = resultats.clone(); // stocke les plants sélectionnés dans une variable globale de la classe.
        return true; // signifie qu'au moins un Plant est sélectionné.
    }

    // 2EME MENU : FORME DES PARCELLES
    public void choixParcelles(int _hauteur, int _largeur) {
        clearFrame(); // nettoie la fenetre
        if (_hauteur < 1 || _largeur < 2) { // verifie les dimensions du potager fournies
            System.out.println("\n entreeUtilisateur() - ERREUR: paramètres h(" + _hauteur + "), l(" + _largeur + ") invalides !\n");
            exit(1);
        }
        System.out.println("2) Saisie des Parcelles");
        int[][] resultat = new int[_hauteur][_largeur]; // tableau des valeurs numériques saisies

        // label d'explication
        final JLabel labelR = new JLabel("Représentez votre Potager :");
        labelR.setBounds(10, 10, 500, 50);
        labelR.setFont(new Font("Arial", Font.PLAIN, 18));
        frame.add(labelR);

        // tableau des zones de texte
        JTextField[][] tabtf = new JTextField[_hauteur][_largeur];
        final int   DX_C = 50, DY_C = 50, // dimensions d'une case
                DX = DX_C*_largeur, DY = DY_C*_hauteur, // dimensions de la grille
                OX = (DIMX-DX)/2, OY = (DIMY-DY-labelR.getHeight())/2; // origines de la grille
        JPanel pGrille = new JPanel(); // grille
        pGrille.setBounds(OX, OY, DX, DY);
        pGrille.setLayout( new GridLayout(_hauteur, _largeur) );
        for (int y = 0; y < _hauteur; y++) {
            for (int x = 0; x < _largeur; x++) {
                tabtf[y][x] = new JTextField();
                tabtf[y][x].setSize(DX_C, DY_C);
                tabtf[y][x].setFont(new Font("Arial", Font.PLAIN, 24));
                pGrille.add(tabtf[y][x]);
            }
        }
        frame.add(pGrille);

        // label d'Erreur
        final JLabel labelE = new JLabel();
        labelE.setBounds(400, 400, 500, 50);
        labelE.setForeground(new Color(200, 0, 0));
        frame.add(labelE);

        // bouton Valider
        final JButton buttonV = boutonValider();
        buttonV.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (testValidesLucas(tabtf, _hauteur, _largeur)) { // verifie si la saisie est correcte
                    for (int y = 0; y < _hauteur; y++) {
                        for (int x = 0; x < _largeur; x++) {
                            resultat[y][x] = Integer.parseInt(tabtf[y][x].getText());
                        }
                    }
                    System.out.println("On passe au Prolog");
                    affichageResultat(_hauteur, _largeur, resultat, true,"");
                } else // si la saisie est incorrecte
                    System.out.println("Saisie Incorrecte");
            }
        });
        frame.add(buttonV);

        // bouton Retour
        final JButton buttonR = boutonRetour();
        buttonR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                initialisationInterface();
            }
        });
        frame.add(buttonR);

        // jusqu'à ce qu'on appuie sur le bouton VALIDER
        frame.setVisible(true);
    } // fin entreeUtilisateur()

    //méthode servant à compter le nombre de parcelles à partir du tableau entré par l'utilisateur et ses dimensions
    private static int countParcel(int[][] tabParcel, int hauteur, int longueur) {
        int compval, k, l;
        boolean doublon;
        int taille = hauteur*longueur;
        for (int i = 0; i < longueur; ++i) {
            for (int j = 0; j < hauteur; ++j) {
                compval = tabParcel[j][i];
                k = i;
                l = j;
                if (l+1 == hauteur) {
                    l = 0;
                    ++k;
                } else {
                    ++l;
                }
                doublon = false;
                while ((k < longueur) && !doublon) {
                    while ((l < hauteur) && !doublon) {
                        if (compval == tabParcel[l][k])
                            doublon = true;
                        ++l;
                    }
                    ++k;
                    l = 0;
                }
                if (doublon)
                    taille--;
            }
        }
        return taille;
    }

    /*
    méthode servant à l'exécution du prolog et à la lecture des solutions depuis prolog
    _hauteur : hauteur de la grille
    _largeur : largeur de la grille
    -tabpotager : la grille entrée par l'utilisateur
    -base : si vrai, exécution de l'algorithme d'optimisation de base, sinon algorithme de modification
    -command : donne les arguments pour la modification de légumes
     */
    public void affichageResultat(int _hauteur, int _largeur, int[][] tabPotager, boolean base, String command) {
        System.out.println("3) Affichage des Resultats");
        affichageEcranChargement(_hauteur, _largeur);
        if(base) {//algorithme d'optimisation de base
            int taillePot = countParcel(tabPotager, _hauteur, _largeur);
            Potager curpotager = new Potager(taillePot);//on crée un nouveau potager

            curpotager.sauvegarderTabProx(tabPotager, taillePot, plantsSelec);//écriture du fichier prox.pl
        }
        try {
            ProcessBuilder pb;
            String contenu;
            if(base)//choix du script à exécuter
                pb = new ProcessBuilder("bash", "scriptPl");
            else {//dans le cas d'une modification de légume, on écrit le script bash ici
                contenu="#!/usr/bin/bash\ncd ..\ncd ..\ncd prolog\n"+command;
                try {
                    FileWriter myWriter = new FileWriter("scriptPl2");
                    myWriter.write(contenu);
                    myWriter.close();
                    System.out.println("Valeur écrite, succès");
                } catch (IOException e) {
                    System.out.println("Erreur lors de l'écriture");
                    e.printStackTrace();
                }
                pb = new ProcessBuilder("bash", "scriptPl2");

            }
            final Process process = pb.start();//on exécute le script et on attend la fin de son exécution
            try{
                process.waitFor();
            }
            catch (Exception ex) {
                System.out.println("Un probleme lors de l'execution du prolog");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        File fileSolution = new File("../../prolog/solution.txt");//on ouvre le fichier des solutions pour le lire
        int indSol = 0;
        String[] sol;
        String data;
        int nbLines=0;
        try {
            Scanner countLine = new Scanner(fileSolution);
            while(countLine.hasNextLine())//on compte le nombre de lignes correspondant au nombre de solutions renvoyées par prolog pour
            {                             //initialiser le tableau contenant les solutions
                ++nbLines;
                countLine.nextLine();
            }
            countLine.close();
            String[][] tabSolution=new String[nbLines][2];
            Scanner readSolution = new Scanner(fileSolution);
            while (readSolution.hasNextLine()) { //on lit les solutions et on les traite pour avoir seulement la liste d'associations parcelle/légume et le score
                data = readSolution.nextLine();
                sol = data.split("\\[");
                sol[1]=sol[1].substring(0,sol[1].length()-1);
                tabSolution[indSol][0]=sol[0];
                tabSolution[indSol][1]=sol[1];
                ++indSol;
            }//on appelle la fonction d'affichage
            if(indSol>0) {
                displaySol(tabSolution, 0, tabPotager, _hauteur, _largeur, base);
            }
        }
        catch (FileNotFoundException exceptionNF) {
            System.out.println("Erreur lors de la lecture des solutions");
            exceptionNF.printStackTrace();
        }
    }

    /*
    affichage graphique des solutions
    -tabsol : tableau des solutions renvoyées par prolog
    -solnumber : la solution actuellement affichée
    -tabParcelles : tableau des entrées utilisateur
    -hauteur : la hauteur du tableau des entrées
    -largeur : sa largeur
    -base : si vrai, algorithme d'optimisaation de base, sinon algorithme de modification
     */
    private void displaySol(String[][] tabSol, int solNumber, int[][] tabParcelles, int hauteur, int largeur, boolean base)
    {
        clearFrame();

        String[] tabAssoc;//tableau des associations parcelles, légumes
        String[] curPair;// tableau de l'association courante (ex : {1,tomate})
        frame.add(header(solNumber, tabSol, tabParcelles, largeur, hauteur)); // ENTETE : numéro de la solution courante,
        // nombre de solution, score calculé et
        // panel des legumes                                                  // bouton de sauvegarde
        JPanel pLegumes = new JPanel();
        pLegumes.setBounds(20, 80, DIMX-20*2, 420);
        pLegumes.setBackground(GRIS_CLAIR);

        GridBagConstraints cont;
        JTextField l1;
        pLegumes.setLayout(new GridBagLayout());
        cont = new GridBagConstraints();
        tabAssoc=tabSol[solNumber][1].split("\\),\\(");
        tabAssoc[0]=tabAssoc[0].substring(1);
        tabAssoc[tabAssoc.length-1]=tabAssoc[tabAssoc.length-1].substring(0,tabAssoc[tabAssoc.length-1].length()-1);
        InterfaceGraphique inter = this;//on récupère la référence de l'interface pour l'envoyer à FenetreModale
        for(int i=0;i<tabAssoc.length;++i)//on parcourt toutes les associations
        {
            curPair=tabAssoc[i].split(",");
            for(int j=0;j<hauteur;++j)//ensuite, on parcourt le tableau des entrées utilisateur
            {
                for(int k=0;k<largeur;++k)
                {
                    if(tabParcelles[j][k]==Integer.parseInt(curPair[0]))//jusqu'à arriver sur une case de l'association courante
                    {
                        if(base) { //si on est sur l'algorithme d'optimisation de base, l'affichage sera quelque peu différent par rapport à une modification
                            for (int icon = 0; icon < Plant.nbPlants(); icon++) {
                                Plant p = Plant.values()[icon];
                                String minuscule = p.getNom().toLowerCase();
                                if (curPair[1].equals(minuscule)) {
                                    JButton bLegume = new JButton(p.getIcon());//on place le bouton du legume correspondant à la case courante du tableau d'entrées
                                    bLegume.setBackground(Color.WHITE);
                                    bLegume.setBounds(j, k, 100, 40);
                                    final String indLeg = curPair[0];
                                    bLegume.addActionListener(new ActionListener() {
                                        @Override
                                        public void actionPerformed(ActionEvent actionEvent) {
                                            tabSolutionTmp = tabSol.clone(); // sauvegarde l'ancien tableau pour l'annulation de la modification
                                            (new FenetreModale(frame, plantsSelec, tabSol[solNumber][1], indLeg, hauteur, largeur, tabParcelles, inter)).setVisible(true);
                                            /*
                                            Affichage d'une fenêtre modale pour la sélection des légumes lors d'une modification
                                            */
                                        }
                                    });
                                    cont.fill = GridBagConstraints.HORIZONTAL;
                                    cont.gridx = k;
                                    cont.gridy = j;
                                    cont.weightx = 0.5;
                                    pLegumes.add(bLegume, cont);
                                }
                            }
                            frame.add(boutonAleatoire(cont, hauteur, largeur, solNumber, tabSol, tabParcelles, base));
                        }
                        else // modification legume
                        {
                            for (int icon = 0; icon < Plant.nbPlants(); icon++) {
                                Plant p = Plant.values()[icon];
                                String minuscule = p.getNom().toLowerCase();
                                if (curPair[1].equals(minuscule)) {
                                    Icon iconleg = p.getIcon();
                                    JLabel lLegume = new JLabel("", JLabel.CENTER);
                                    lLegume.setIcon(iconleg);
                                    lLegume.setBackground(Color.WHITE);
                                    lLegume.setBorder(BorderFactory.createLineBorder(GRIS_FONCE, 2));
                                    lLegume.setBounds(j, k, 100, 40);
                                    cont.fill = GridBagConstraints.HORIZONTAL;
                                    cont.gridx = k;
                                    cont.gridy = j;
                                    cont.weightx = 0.5;
                                    pLegumes.add(lLegume, cont);
                                }
                            }
                            JButton annulerChgt = bouton("Annuler Changement", JAUNE);
                            final int DX = 240, DY = 40;
                            annulerChgt.setBounds(DIMX/2-(140+DX), DIMY-DY*2 - 20, DX-40, DY);
                            annulerChgt.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent actionEvent) {
                                    displaySol(tabSolutionTmp, 0, tabParcelles, hauteur, largeur, true);
                                }
                            });
                            frame.add(annulerChgt);
                        } // else
                    } // if
                } // for
            } // for
        } // for

        frame.add(pLegumes);
        if (solNumber != 0) // si PAS premiere solution
            frame.add(boutonPrecedent(cont, hauteur, largeur, solNumber, tabSol, tabParcelles, base), cont);
        if (solNumber != tabSol.length-1) // si PAS derniere solution
            frame.add(boutonSuivant(cont, hauteur, largeur, solNumber, tabSol, tabParcelles, base), cont);
        frame.add(boutonTerminer(cont, hauteur, largeur), cont);

        frame.setVisible(true);
    }

    // ENTETE DE L'AFFICHAGE DES SOLUTIONS
    private JPanel header(final int _solNumber, final String[][] _tabSol, final int[][] _tabParcelles, final int _largeur, final int _hauteur) {
        JPanel pHeader = new JPanel();
        pHeader.setBounds(130, 20, DIMX-130*2, 40 );
        pHeader.setBackground(GRIS_CLAIR);
        pHeader.setLayout( new GridLayout(1, 3) );
        pHeader.add( new JLabel("Solution "+ (_solNumber+1)+ "/"+ _tabSol.length) );
        pHeader.add( new JLabel("SCORE: "+ _tabSol[_solNumber][0]) ); // affichage du score de cette config
        JButton boutonSauvegarder = bouton("SAUVEGARDER", VERT);
        boutonSauvegarder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.out.println("Exportation en fichier Excel...");
                String title = "resultatsPlantation" + _solNumber + ".csv";
                try{
                    String[] tabAssoc;
                    String[] curPair;
                    FileWriter fileLeg = new FileWriter(title);
                    System.out.println("Exportation en fichier Excel...");
                    tabAssoc = _tabSol[_solNumber][1].split("\\),\\(");
                    tabAssoc[0] = tabAssoc[0].substring(1);
                    tabAssoc[tabAssoc.length-1]=tabAssoc[tabAssoc.length-1].substring(0,tabAssoc[tabAssoc.length-1].length()-1);

                    int legumeCourant;
                    int l;
                    for (int j = 0; j < _hauteur; ++j){
                        for(int k = 0; k < _largeur; ++k){
                            l=0;
                            legumeCourant = _tabParcelles[j][k];
                            while((l<tabAssoc.length)&&(Integer.parseInt(tabAssoc[l].split(",")[0])!=legumeCourant)){
                                ++l;
                                curPair=tabAssoc[l].split(",");
                            }
                            fileLeg.append(tabAssoc[l].split(",")[1]);
                            fileLeg.append(",");
                            if(k == _largeur -1){
                                fileLeg.append("\n");
                            }
                        }
                    }
                    fileLeg.close();
                    (new JOptionPane()).showMessageDialog(frame, "Exportation terminée avec succès !");
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
        pHeader.add( boutonSauvegarder );
        return pHeader;
    }

    // ECRAN DE CHARGEMENT (pendant le calcul du prolog)
    private void affichageEcranChargement(final int _hauteur, final int _largeur) {
        clearFrame();
        final JPanel pChargemement = new JPanel();
        pChargemement.setSize(DIMX, DIMY);
        final JLabel lChargement = new JLabel("CHARGEMENT DES SOLUTIONS<br>EN COURS ...");
        pChargemement.add(lChargement);
        final JImagePanel iChargement = new JImagePanel("../resources/images/loading.gif");
        pChargemement.add(iChargement);
        final JButton bRetour = boutonRetour();
        bRetour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                choixParcelles(_hauteur, _largeur); // retour au dimensionnement des parcelles
            }
        });
        pChargemement.add(bRetour);
        frame.add(pChargemement);
    }


    // teste si les valeurs rentrées par l'utilisateur sont correctes
    private boolean testValidesLucas(final JTextField[][] _tabtf, final int _hauteur, final int _largeur) {
        for (int y = 0; y < _hauteur; y++) {
            for (int x = 0; x < _largeur; x++) {
                if (_tabtf[y][x].getText().equals(""))
                    return false;
            }
        }
        return true;
        // pour l'instant vérifie seulement si l'utilisateur n'a pas mis de vide.
    }



    private JButton boutonRetour() {
        JButton resultat = bouton("RETOUR", ROUGE);
        resultat.setBounds(20, DIMY-100, 100, 40);
        return resultat;
    }

    private JButton boutonValider() {
        JButton resultat = bouton("VALIDER", VERT);
        resultat.setBounds(DIMX-120, DIMY-100, 100, 40);
        return resultat;
    }

    private JButton boutonAleatoire(final GridBagConstraints _cont, final int _hauteur, final int _largeur, final int _solNumber, final String[][] _tabSol, final int[][] _tabParcelles, boolean base) {
        JButton b = bouton("Aléatoire", JAUNE);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                displaySol(_tabSol, (int)(Math.random()* _tabSol.length), _tabParcelles, _hauteur, _largeur, base);//affichage d'une solution aléatoire
            }
        });
        final int DX = 120, DY = 40;
        b.setBounds(DIMX/2-(5+DX)-(20+DX), DIMY-DY*2 - 20, DX, DY);
        _cont.fill = GridBagConstraints.SOUTHWEST;
        _cont.gridy = 0;
        _cont.gridx = _largeur-1;
        return b;
    }

    private JButton boutonPrecedent(final GridBagConstraints _cont, final int _hauteur, final int _largeur, final int _solNumber, final String[][] _tabSol, final int[][] _tabParcelles, boolean base) {
        JButton b = bouton("◄ Précédent", GRIS_FONCE);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                displaySol(_tabSol, _solNumber-1, _tabParcelles, _hauteur, _largeur, base);
            }
        });
        final int DX = 120, DY = 40;
        b.setBounds(DIMX/2-(5+DX), DIMY-DY*2-20, DX, DY);
        _cont.fill = GridBagConstraints.SOUTHWEST;
        _cont.gridy = _hauteur;
        _cont.gridx = 0;
        return b;
    }

    private JButton boutonSuivant(final GridBagConstraints _cont, final int _hauteur, final int _largeur, final int _solNumber, final String[][] _tabSol, final int[][] _tabParcelles, boolean base) {
        JButton b = bouton("Suivant ►", GRIS_FONCE);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                displaySol(_tabSol, _solNumber+1, _tabParcelles, _hauteur, _largeur, base);
            }
        });
        final int DX = 120, DY = 40;
        b.setBounds(DIMX/2+(5), DIMY-DY*2-20, DX, DY);
        _cont.fill = GridBagConstraints.SOUTH;
        _cont.gridy = _hauteur;
        _cont.gridx = 0;
        return b;
    }

    private JButton boutonTerminer(final GridBagConstraints _cont, final int _hauteur, final int _largeur) {
        JButton b = bouton("TERMINER", ROUGE);
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                initialisationInterface();
            }
        });
        final int DX = 120, DY = 40;
        b.setBounds(DIMX/2+(5)+(20+DX), DIMY-DY*2-20, DX, DY);
        _cont.fill = GridBagConstraints.SOUTHEAST;
        _cont.gridy = _hauteur;
        _cont.gridx = _largeur-1;
        return b;
    }

    private static JButton bouton(final String _text, final Color _color) {
        JButton resultat = new JButton(_text);
        resultat.setBackground(_color);
        resultat.setForeground(Color.WHITE);
        return resultat;
    }

    private static final Color
            GRIS_FONCE = new Color(90, 90, 90),
            GRIS_CLAIR = new Color(200, 200, 200),
            JAUNE = new Color(255, 183, 0),
            ROUGE = new Color(200, 0, 0),
            VERT = new Color(40, 140, 40);

    // efface tout le contenu de la fenetre
    private void clearFrame() {
        frame.getContentPane().removeAll();
        frame.repaint();
    }

}
