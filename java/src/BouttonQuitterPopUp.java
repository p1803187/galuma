import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static java.lang.System.exit;

//class de la fenetre pop up de l'accueil et de la fermeture de l'application
public class BouttonQuitterPopUp extends JDialog implements ActionListener {

	JButton Valider;
	JButton Annuler;

	BouttonQuitterPopUp(JFrame f) { //constructeur
		super(f,"Attention",true);
		setPreferredSize(new Dimension(400,300)); //redimensionner la fenêtre
		pack();
		setLocationRelativeTo(f);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);//empecher la fermeture de la fenetre par la croix

		Container c= getContentPane();
		JImagePanel img = new JImagePanel("../resources/images/warning.png");
		img.setBounds(30,60, 90,100);
		//Centrer l'image
		img.setStretch(false);
		c.add(img);

		// warning
		JLabel l1;
		l1= new JLabel("Etes-vous sûr de vouloir quitter ?");
		l1.setBounds(130,70, 350,100);
		c.add(l1);

		c.setLayout(new BorderLayout());
		c.add(panneau_bas(), BorderLayout.SOUTH);
	}

	JPanel panneau_bas(){
		JPanel bas = new JPanel();
		Valider = new JButton("Valider");
		Annuler = new JButton("Annuler");
		bas.add(Valider);
		bas.add(Annuler);
		Valider.addActionListener(this);
		Annuler.addActionListener(this);
		return bas;
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == Valider){
			dispose();//ferme la fenetre et l'application
			exit(0);
		} else if(e.getSource() == Annuler){
			dispose();//ferme la pop up
		}
	}

}
