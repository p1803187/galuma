import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//classe pour la fenetre de changement de légume
public class FenetreModale extends JDialog implements ActionListener {

    JButton Valider;
    JButton Annuler;
    boolean okChoisi;
    JCheckBox[] tabLeg = new JCheckBox[Plant.nbPlants()];
    String[] resultat = new String[Plant.nbPlants()];
    int i = 0;
    int j = 0;
    Plant[] plantesPrim;
    String tabAdjacence;
    String indPara;
    int hauteurT;
    int largeurT;
    int[][] tabPot;
    Plant[] legumeSelectionne;
    InterfaceGraphique interF;

    //constructeur
    public FenetreModale(JFrame f, Plant[] plantesBase, String tabAdj, String indiceChangement, int hauteur, int largeur, int[][] tabPotager, InterfaceGraphique inter) {
        super(f, "choix legume de remplacement", true);
        setPreferredSize(new Dimension(600,600)); //redimensionner la fenêtre
        pack();
        setLocationRelativeTo(f);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);//ferme la fenetre si on appuie sur la croix
        Container c = getContentPane();
        c.setLayout(new BorderLayout());
        c.add(panneau_bas(), BorderLayout.SOUTH);
        c.add(panneau_milieu(), BorderLayout.CENTER);
        plantesPrim=plantesBase;//les paramètres nécessaires pour l'appel de la modification des légumes
        tabAdjacence=tabAdj;
        indPara=indiceChangement;
        hauteurT=hauteur;
        largeurT=largeur;
        tabPot=tabPotager;
        interF=inter;
    }

    private JPanel panneau_milieu() {//def panneau mid

        JPanel leg = new JPanel();
        leg.setLayout(new GridLayout(1,1,20,20));

        final int OX=320, OY=50, DX=220, DY=420, IX=15, B=5;

        // legumes
        final JPanel panel1 = new JPanel();
        panel1.setBounds(OX-B, OY-B, DX+B, DY+B);
        panel1.setBackground(new Color(200, 200, 200));

        // herbes
        final JPanel panel2 = new JPanel();
        panel2.setBounds(OX-B+DX+IX, OY-B, DX+B, DY+B);
        panel2.setBackground(new Color(200, 200, 200));

        // affectation des checkboxes
        for (Plant p: Plant.values()) {
            if (p.getIndex() <= 28) {
                JCheckBox tmp = new JCheckBox(p.getNom());
                tabLeg[i] = tmp;
                resultat[i] = p.getNom();
                panel1.add(tabLeg[i]);
                i++;
            } else {
                JCheckBox t = new JCheckBox(p.getNom());
                tabLeg[i] = t;
                resultat[i] = p.getNom();
                panel2.add(tabLeg[i]);
                i++;
            }
        }

        leg.add(panel1);
        leg.add(panel2);

        return leg;
    }

    private JPanel panneau_bas(){
        JPanel bas = new JPanel();
        Valider = new JButton("Valider");
        Annuler = new JButton("Annuler");
        bas.add(Valider);
        bas.add(Annuler);
        Valider.addActionListener(this);
        Annuler.addActionListener(this);
        return bas;
    }

    //pour savoir si un plant est séléctionner
    public boolean plantsUtiles(final JCheckBox[] tabcb) {
        int nbs = 0;
        for (JCheckBox cb: tabcb) {
            if (cb.isSelected())
                nbs++;
        }
        if (nbs < 1) {
            JOptionPane.showMessageDialog(this, "Veuillez sélectionner au moins un Plant.");
            return false;
        }
        legumeSelectionne = new Plant[nbs];
        int n = 0;
        for (int i = 0; i < nbs; i++) {
            while (n < tabcb.length && !tabcb[n].isSelected())
                n++;

            if(n<tabcb.length) {
                legumeSelectionne[i] = Plant.values()[n];
                ++n;
            }
        }
        return true;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Valider) {//lorsqu'on a fini de sélectionner les plantes pour la modification
            if (!plantsUtiles(tabLeg))
                return;
            Potager.sauvegarderModif(legumeSelectionne, plantesPrim);//mise à jour des fichiers prolog pour la modification
            String commandeProlog="swipl -s test_legumes.pl -g \"soluceFile2(["+tabAdjacence+"],"+indPara+"),halt.\"";//définition des arguments pour prolog
            interF.affichageResultat(hauteurT, largeurT, tabPot,false, commandeProlog);
            okChoisi = true;
            dispose();
        } else if (e.getSource() == Annuler) {
            okChoisi = false;
            dispose();
        }
    }
}
