
public class Parcelle {

    private final static Parcelle NULL = new Parcelle();

    private Plant plant;
    private Parcelle[] voisins;

    // constructeur par defaut
    public Parcelle() {
        voisins = new Parcelle[]{NULL, NULL, NULL, NULL};
    }

    // constructeur avec voisins
    public Parcelle(final Parcelle[] VOISINS) {
        voisins = VOISINS.clone();
    }

    // constructeur avec voisins et plant
    public Parcelle(final Parcelle[] VOISINS, final Plant PLANT) {
        voisins = VOISINS.clone();
        plant = PLANT;
    }

    // constructeur par copie
    public Parcelle(final Parcelle PARCELLE) {
        voisins = PARCELLE.voisins.clone();
        plant = PARCELLE.plant;
    }

    public void setPlant(final Plant PLANT) { plant = PLANT; }

    public Plant getPlant() { return plant; }

}
