
// Ceci importe la classe Scanner du package java.util
// import java.util.Scanner;
// Ceci importe toutes les classes du package java.util
import java.util.*;
/*Classe AffichageConsole pour la première version de l'application, servant à lire les entrées de l'utilisateur qui étaient ensuite utilisées par
Main.mainAffichageConsole() pour l'affichage
*/
public class AffichageConsole {
    private static int hauteur;
    public static int longueur;
    public AffichageConsole()
    {
        longueur=0;
        hauteur=0;
    }
    public int getHauteur() {
        return hauteur;
    }

    public int getLongueur() {
        return longueur;
    }

    public static int[][] affichage() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Veuillez saisir le nombre de parcelle en longueur de votre terrain :");
        longueur = sc.nextInt();
        System.out.println("Veuillez saisir le nombre de parcelle en profondeur de votre terrain :");
        hauteur = sc.nextInt();
        int[][] terrain = new int [longueur][hauteur];

        System.out.println("Votre parcelle fait : " + longueur + " x " + hauteur + " \n \n schématisez votre terrain ci dessous comme l'exemple qui suit : \n \n "
                + " pour une parcelle qui fait deux de longeur faite par exemple 1 1 2 \n"
                + " ici 1 est une parcelle de deux de longueur et 2 une parcelle de 1 de longeur \n"
                + " par exemple si votre terrain fait 4 par 3 \n et que vous avez la première parcelle de deux de longueur \n \n"
                + " votre terrain sera du style: \n  1 1 2 3 \n 4 5 6 7 \n \n"
                + " par exemple si votre terrain fait 4 par 3 \n et que vous avez la première parcelle de deux de profondeur \n \n"
                + " votre terrain sera du style: \n 1 2 3 4 \n 1 5 6 7 \n \n "
                + " A vous de jouez, a chaque fin de ligne appuyer sur entrée et saississez la seconde ligne etc... \n la fin de saisie de ligne est automatique ");

        int taille = 0 ;

        Scanner scan = new Scanner(System.in);
        System.out.println(" Saisissez vos parcelles : ");
        String parcelle = scan.nextLine();
        String[] p = parcelle.split(" ");
        if (p.length != longueur) {
            System.out.println(" Mauvaise saisie. Veuillez relancer l'application. ");
            System.exit(1);
        }

        while (taille != (hauteur-1)) {
            for (int i = 0; i < longueur; i++){
                terrain[i][taille] = Integer.parseInt(p[i]);
            }

            taille = taille + 1 ;
            parcelle = scan.nextLine();
            p = parcelle.split(" ");
            if (taille == hauteur-1) {
                for (int i = 0; i < longueur; i++) {
                    terrain[i][taille] = Integer.parseInt(p[i]);
                }
            }
        }

        System.out.println(" Votre parcelle est du type :");
        for (int j = 0; j < hauteur; j++) {
            for (int i = 0; i < longueur; i++) {
                System.out.print(terrain[i][j] + " ");
            }
            System.out.print(" \n");
        }
        return terrain;

    } // fin de affichage()

}
