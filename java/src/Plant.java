// https://www.permatheque.fr/2015/02/15/associations-de-plantes-au-potager/

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public enum Plant {

    // legumes
    AIL(1, "Ail", Groupe.BULBE, Saison.PRINTEMPS, Climat.OCEANIQUE,"../resources/images/Ail.png","À planter dans un sol riche et avec ensoleillement"),
    ASPERGE(2, "Asperge", Groupe.TIGES, Saison.PRINTEMPS, Climat.MEDITERANEEN,"../resources/images/Asperges.png","n'aime pas le froid, et favoriser le fumier comme engrais"),
    AUBERGINE(3, "Aubergine", Groupe.FRUITS, Saison.HIVER, Climat.MEDITERANEEN,"../resources/images/Aubergine.png","A besoin d'un sol riche et chaud"),
    BETTERAVE(4, "Betterave", Groupe.RACINES, Saison.ETE, Climat.OCEANIQUE,"../resources/images/Betterave.png","A besoin de compost par griffage"),
    CAROTTE(5, "Carotte", Groupe.RACINES, Saison.HIVER, Climat.CONTINENTAL,"../resources/images/Carotte.png","À planter dans une terre sableuse et profonde"),
    CELERI(6, "Celeri", Groupe.TIGES, Saison.PRINTEMPS, Climat.OCEANIQUE,"../resources/images/Céleri.png","À planter dans un sol limino-argileux et au soleil"),
    CHOU(7, "Chou", Groupe.FEUILLES, Saison.HIVER, Climat.CONTINENTAL,"../resources/images/Chou.png","À planter dans un sol riche en humus et argileux et au soleil"),
    CHOU_FLEUR(8, "Chou-fleur", Groupe.FLEURS, Saison.PRINTEMPS, Climat.OCEANIQUE,"../resources/images/Chou-fleur.png","À planter dans un sol riche en humus et argileux-calcaires et au soleil"),
    CONCOMBRE(9, "Concombre", Groupe.FRUITS, Saison.PRINTEMPS, Climat.CONTINENTAL,"../resources/images/Concombre.png","À planter dans un sol riche en humus avec du fumier et au soleil"),
    COURGE(10, "Courge", Groupe.FRUITS, Saison.ETE, Climat.MEDITERANEEN,"../resources/images/Courge.png","À planter dans un milieu sec et tempéré et chaud, et dans un sol souple et frais"),
    COURGETTE(11, "Courgette", Groupe.FRUITS, Saison.ETE, Climat.CONTINENTAL,"../resources/images/Courgette.png","À planter dans un milieu sec et tempéré et chaud, et dans un sol souple et frais"),
    ECHALOTTE(12, "Echalotte", Groupe.BULBE, Saison.PRINTEMPS, Climat.MEDITERANEEN, "../resources/images/Echalote.png","À planter plein soleil dans un sol sabloneux et bien drainé"),
    EPINARD(13, "Epinard", Groupe.FEUILLES, Saison.ETE, Climat.OCEANIQUE, "../resources/images/Epinard.png","À planter dans un sol humifère et à mi-ombre"),
    FEVE(14, "Feve", Groupe.GRAINES, Saison.AUTOMNE, Climat.OCEANIQUE, "../resources/images/Feve.png","À planter en plein soleil. Ils supportent la salinité"),
    FENOUIL(15, "Fenouil", Groupe.TIGES, Saison.PRINTEMPS, Climat.MEDITERANEEN,"../resources/images/Fenouil.png","À planter en plein soleil, dans un sol en humus, leger et frais"),
    FRAISIER(16, "Fraisier", Groupe.FRUITS, Saison.ETE, Climat.MEDITERANEEN,"../resources/images/Fraise.png","Aime le soleil mais aussi la mi-ombre, à planter dans une terre riche"),
    HARICOT(17, "Haricot", Groupe.GRAINES, Saison.PRINTEMPS, Climat.OCEANIQUE,"../resources/images/Haricots.png","À exposer au soleil, dans un sol humifère, acompagné des températures douces"),
    LAITUE(18, "Laitue", Groupe.FEUILLES, Saison.PRINTEMPS, Climat.MEDITERANEEN,"../resources/images/Laitue.png","À planter dans un sol riches en humus, n'aime pas trop le soleil."),
    MELON(19, "Melon", Groupe.FRUITS, Saison.PRINTEMPS, Climat.OCEANIQUE,"../resources/images/Melon.png","À planter en plein soleil, dans un sol riche en humus avec du fumier et à l'abri"),
    NAVET(20, "Navet", Groupe.RACINES, Saison.PRINTEMPS, Climat.CONTINENTAL,"../resources/images/Navet.png","À planter à la mi-ombre, dans un sol riche en humus et frais"),
    OIGNON(21, "Oignon", Groupe.BULBE, Saison.PRINTEMPS, Climat.OCEANIQUE,"../resources/images/Oignon.png","À planter en plein soleil, dans un sol bien drainé sabloneux et sensible au mouche oignon"),
    POIREAU(22, "Poireau", Groupe.FEUILLES, Saison.PRINTEMPS, Climat.MEDITERANEEN,"../resources/images/Poireau.png","À planter en plein soleil, dans un sol bien drainé sabloneux"),
    POIS(23, "Pois", Groupe.GRAINES, Saison.PRINTEMPS, Climat.OCEANIQUE,"../resources/images/Pois.png","À planter en plein soleil et dans un sol riche en humus"),
    POIVRON(24, "Poivron", Groupe.FRUITS, Saison.PRINTEMPS, Climat.CONTINENTAL,"../resources/images/Poivron.png","À planter en plein soleil et dans un sol riche en humus, sensible au puceron"),
    PATATE(25, "Patate", Groupe.TUBERCULES, Saison.PRINTEMPS, Climat.OCEANIQUE,"../resources/images/Patate.png","À planter au soleil et dans un sol riche en humus"),
    RADIS(26, "Radis", Groupe.RACINES, Saison.AUTOMNE, Climat.CONTINENTAL,"../resources/images/Radis.png","À planter mi-ombre et dans des températures fraîches"),
    ROQUETTE(27, "Roquette", Groupe.FEUILLES, Saison.AUTOMNE, Climat.OCEANIQUE,"../resources/images/Roquette.png","À planter au soleil ou mi-ombre, dans un sol normal et résiste aux hivers rudes"),
    TOMATE(28, "Tomate", Groupe.FRUITS, Saison.ETE, Climat.MEDITERANEEN,"../resources/images/Tomate.png","À planter dans des sols fertiles, sous un gros soleil et beaucoup de chaleur"),

    // herbes
    ABSINTHE(29, "Absinthe", Groupe.MEDICINALES, Saison.AUTOMNE, Climat.MEDITERANEEN,"../resources/images/Absinthe.png","Plante vivace, toxique à forte dose. À planter au soleil avec sol riche en humus, resiste bien au froid"),
    ARMOISE(30, "Armoise", Groupe.AROMATES, Saison.AUTOMNE, Climat.CONTINENTAL,"../resources/images/Armoise.png","À planter au soleil dans un sol ordinaire"),
    ANETH(31, "Aneth", Groupe.AROMATES, Saison.PRINTEMPS, Climat.OCEANIQUE,"../resources/images/Aneth.png","Aime la chaleur et le soleil mais est à planter dans un sol frais et riche"),
    BASILIC(32, "Basilic", Groupe.AROMATES, Saison.PRINTEMPS, Climat.MEDITERANEEN,"../resources/images/Basilic.png","À planter au soleil ou mi-ombre dans un sol riche en humus et bien drainé"),
    BOURRACHE(33, "Bourrache", Groupe.MEDICINALES, Saison.PRINTEMPS, Climat.OCEANIQUE,"../resources/images/Bourrache.png","À exposer en plein soleil, dans un sol riche et profond"),
    CAPUCINE(34, "Capucine", Groupe.FLEURS, Saison.HIVER, Climat.CONTINENTAL,"../resources/images/Capucine.png","À planter à mi-ombre dans un sol riche en humifère"),
    CIBOULETTE(35, "Ciboulette", Groupe.AROMATES, Saison.PRINTEMPS, Climat.OCEANIQUE,"../resources/images/Ciboulette.png","À exposer plein soleil dans un sol frais"),
    CORIANDRE(36, "Coriandre", Groupe.AROMATES, Saison.HIVER, Climat.MEDITERANEEN,"../resources/images/Coriandre.png","À exposer plein soleil dans n'importe quel sol"),
    LIN(37, "Lin", Groupe.FLEURS, Saison.ETE, Climat.CONTINENTAL,"../resources/images/Lin.png",""),
    MENTHE(38, "Menthe", Groupe.AROMATES, Saison.ETE, Climat.CONTINENTAL,"../resources/images/Menthe.png","Aime le soleil et la mi-ombre et les sols humifères"),
    OEILLET_D_INDE(39, "Oeillet", Groupe.FLEURS, Saison.AUTOMNE, Climat.MEDITERANEEN,"../resources/images/Oeillet.png","À planter à mi-ombre dans un sol sabloneux-argileux et réguièrement aroser"),
    PERSIL(40, "Persil", Groupe.AROMATES, Saison.PRINTEMPS, Climat.CONTINENTAL,"../resources/images/Persil.png","Aime la mi-ombre et les sols frais riche en humus"),
    RAIFORT(41, "Raifort", Groupe.AROMATES, Saison.AUTOMNE, Climat.OCEANIQUE,"../resources/images/Raifort.png","À planter en plein soleil dans un sol riche en humus et bien drainé"),
    ROMARAIN(42, "Romarain", Groupe.AROMATES, Saison.PRINTEMPS, Climat.MEDITERANEEN,"../resources/images/Romarain.png","Aime le soleil et des sols pauvres, arrides"),
    SARIETTE(43, "Sariette", Groupe.AROMATES, Saison.ETE, Climat.OCEANIQUE,"../resources/images/Sariette.png","À planter en plein soleil dans un sol bien drainé"),
    SAUGE(44, "Sauge", Groupe.MEDICINALES, Saison.PRINTEMPS, Climat.CONTINENTAL,"../resources/images/Sauge.png","A exposer plein soleil dans un sol souple et riche"),
    SOUCI(45, "Souci", Groupe.FLEURS, Saison.PRINTEMPS, Climat.MEDITERANEEN,"../resources/images/Souci.png","À planter mi-ombre, dans un sol ordinaire"),
    TABAC_D_ORNEMENT(46, "Tabac", Groupe.FLEURS, Saison.PRINTEMPS, Climat.OCEANIQUE, "../resources/images/Tabac.png","À planter au soleil, dans un sol riche en humus et resiste au froid"),
    THYM(47, "Thym", Groupe.AROMATES, Saison.PRINTEMPS, Climat.MEDITERANEEN,"../resources/images/Thym.png","A exposer plein soleil dans un sol sec");



    private final int index; // numéro du plant pour le retrouver dans le tableau de synergies
    private final String nom;
    private final Groupe groupe;
    private final Saison saison;
    private final Climat climat;
    private final String icon;
    private final String description;

    // classifications pour les filtres
    public enum Groupe {
        AROMATES, BULBE, FEUILLES, FLEURS, FRUITS, GRAINES, MEDICINALES, RACINES, TIGES, TUBERCULES
    }
    public enum Saison{
        ETE, HIVER, AUTOMNE, PRINTEMPS
    }
    public enum Climat{
        MEDITERANEEN, CONTINENTAL, OCEANIQUE
    }



    // constructeur
    Plant(final int INDEX, final String NOM, final Groupe GROUPE, final Saison SAISON, final Climat CLIMAT,final String ICON,final String DESCRIPTION) {
        index = INDEX;
        nom = NOM;
        groupe = GROUPE;
        saison = SAISON;
        climat = CLIMAT;
        icon = ICON;
        description = DESCRIPTION;
    }

    // renvoie le Score de Synergie
    public static int getScore(final Plant _plant1, final Plant _plant2) {
        return synergies[_plant1.getIndex()-1][_plant2.getIndex()-1];
    }

    // Scores de Synergie possibles
    private static final int M = -1, N = 0, B = +1;

    // tableau de synergies
    private final static int synergies[][] =
            {{ N, B, N, B, B, N, M, M, N, N, N, N, N, M, M, B, M, B, N, N, N, M, M, N, B, N, N, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ B, N, N, N, N, N, N, N, B, N, N, N, N, N, M, N, B, B, N, N, N, B, B, N, N, N, N, B, N, N, N, N, N, N, N, N, N, N, N, B, N, N, N, N, N, N, N}
            ,{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, M, N, B, N, N, N, N, N, N, B, M, B, B, M, N, N, N, N, N, B, N, N, N, N, B, N, N, N, N, N, B, B, B}
            ,{ B, N, N, N, M, B, B, B, N, N, N, B, B, N, M, N, B, B, N, N, B, M, N, N, M, B, B, M, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ B, N, N, M, N, B, B, B, N, N, N, B, B, B, M, N, B, B, N, N, B, B, B, N, M, B, B, B, N, N, N, N, N, N, N, B, N, N, N, N, N, B, N, B, N, N, N}
            ,{ N, N, N, B, B, N, B, B, B, N, N, N, N, B, M, N, B, M, N, N, N, B, B, B, M, B, B, B, N, N, N, N, N, N, B, N, N, N, N, B, B, N, N, N, N, N, N}
            ,{ M, N, N, B, B, B, N, N, B, N, N, M, B, B, M, M, B, B, N, N, M, M, B, N, B, M, M, B, B, B, B, N, B, B, N, B, N, B, B, B, N, B, B, B, B, B, B}
            ,{ M, N, N, B, B, B, N, N, B, N, N, M, B, B, M, M, B, B, N, N, M, M, B, N, B, M, M, B, B, B, B, N, B, B, N, B, N, B, B, B, N, B, B, B, B, B, B}
            ,{ N, B, N, N, N, B, B, B, N, N, N, N, B, B, M, N, B, B, M, N, B, N, B, N, M, N, N, M, N, N, N, B, N, B, N, N, N, N, B, N, N, N, N, M, B, B, N}
            ,{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, M, N, B, B, M, N, B, N, N, N, N, M, M, M, N, N, N, B, N, N, N, N, N, N, B, N, N, N, N, B, B, N, B}
            ,{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, M, N, B, B, M, N, B, N, N, N, N, M, M, M, N, N, N, B, N, N, N, N, N, N, B, N, N, N, N, B, B, N, B}
            ,{ N, N, N, B, B, N, M, M, N, N, N, N, N, N, M, B, M, B, N, N, B, N, M, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, N, B, B, N, B, B, B, N, N, N, N, N, M, B, B, B, B, B, N, B, B, N, M, B, B, B, N, N, N, N, N, N, N, N, N, N, B, N, N, N, N, N, B, N, B}
            ,{ M, N, N, N, B, B, B, B, B, N, N, N, N, N, M, N, B, B, N, N, M, M, M, N, N, N, N, N, N, N, B, N, N, N, N, N, N, N, N, N, N, N, B, N, N, N, N}
            ,{ M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M, M}
            ,{ B, N, N, N, N, N, M, M, N, N, N, B, B, N, M, N, B, B, N, N, B, B, N, N, N, N, N, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ M, B, B, B, B, B, B, B, B, B, B, M, B, B, M, B, N, B, B, B, M, M, M, N, B, B, B, N, N, N, B, N, N, N, N, N, N, N, B, N, N, B, B, N, B, N, N}
            ,{ B, B, N, B, B, M, B, B, B, B, B, B, B, B, M, B, B, N, B, B, B, B, B, N, N, B, B, N, B, B, B, N, N, N, N, N, N, N, N, N, N, B, N, B, B, N, B}
            ,{ N, N, N, N, N, N, N, N, M, M, M, N, B, N, M, N, B, B, N, N, N, N, B, N, M, N, N, N, N, N, N, N, N, N, N, N, N, N, N, B, N, N, N, N, N, N, B}
            ,{ N, N, N, N, N, N, N, N, N, N, N, N, B, N, M, N, B, B, N, N, M, N, B, N, N, N, N, B, N, N, B, N, N, N, N, B, N, B, B, N, N, B, B, B, N, N, B}
            ,{ N, N, N, B, B, N, M, M, B, B, B, B, N, M, M, B, M, B, N, M, N, B, M, N, M, N, N, B, N, N, N, N, N, N, N, N, N, N, N, B, N, N, N, M, N, N, N}
            ,{ M, B, N, M, B, B, M, M, N, N, N, N, B, M, M, B, M, B, N, N, B, N, M, N, N, N, N, B, B, B, N, N, N, N, N, N, N, N, N, B, N, N, N, N, N, N, N}
            ,{ M, B, N, N, B, B, B, B, B, N, N, M, B, M, M, N, M, B, B, B, M, M, N, N, B, B, B, M, B, B, N, N, N, N, N, N, N, N, N, B, N, N, N, N, N, B, N}
            ,{ N, N, B, N, N, N, N, N, N, N, N, N, N, N, M, N, N, N, N, N, N, N, N, N, M, B, B, B, N, N, N, N, N, N, N, N, N, N, B, N, N, N, N, N, B, N, N}
            ,{ B, N, M, M, M, M, B, B, M, N, N, N, M, N, M, N, B, N, M, N, M, N, B, M, N, M, M, M, N, N, N, N, N, N, B, B, B, N, N, N, B, N, N, N, N, N, N}
            ,{ N, N, B, B, B, B, M, M, N, M, M, N, B, N, M, N, B, B, N, N, N, N, B, B, M, N, N, B, N, N, B, N, N, N, N, B, N, B, B, N, N, B, B, B, N, N, B}
            ,{ N, N, B, B, B, B, M, M, N, M, M, N, B, N, M, N, B, B, N, N, N, N, B, B, M, N, N, B, N, N, B, N, N, N, N, B, N, B, B, N, N, B, B, B, N, N, B}
            ,{ B, B, M, M, B, B, B, B, M, M, M, N, B, N, M, B, N, N, N, B, B, B, M, B, M, B, B, N, N, N, B, N, N, B, N, N, N, B, B, B, N, N, N, N, B, B, N}
            ,{ N, N, N, N, N, N, B, B, N, N, N, N, N, N, M, N, N, B, N, N, N, B, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, N, N, N, N, B, B, N, N, N, N, N, N, M, N, N, B, N, N, N, B, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, N, N, N, N, B, B, N, N, N, N, N, B, M, N, B, B, N, B, N, N, N, N, N, B, B, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, N, N, N, N, N, N, B, B, B, N, N, N, M, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, N, N, N, N, B, B, N, N, N, N, N, N, M, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, B, N, N, N, B, B, B, N, N, N, N, N, M, N, N, N, N, N, N, N, N, N, N, N, N, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, N, N, N, B, N, N, N, N, N, N, N, N, M, N, N, N, N, N, N, N, N, N, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, N, B, B, N, B, B, N, N, N, N, N, N, M, N, N, N, N, B, N, N, N, N, B, B, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, N, N, N, N, N, N, N, N, N, N, N, N, M, N, N, N, N, N, N, N, N, N, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, N, N, N, N, B, B, N, N, N, N, N, N, M, N, N, N, N, B, N, N, N, N, N, B, B, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, B, N, N, N, B, B, B, B, B, N, B, N, M, N, B, N, N, B, N, N, N, B, N, B, B, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, B, N, N, N, B, B, B, N, N, N, N, N, N, M, N, N, N, B, N, B, B, B, N, N, N, N, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, N, N, N, B, N, N, N, N, N, N, N, N, M, N, N, N, N, N, N, N, N, N, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, N, N, B, N, B, B, N, N, N, N, N, N, M, N, B, B, N, B, N, N, N, N, N, B, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, N, N, N, N, B, B, N, N, N, N, N, B, M, N, B, N, N, B, N, N, N, N, N, B, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, M, N, N, N}
            ,{ N, N, N, N, B, N, B, B, M, B, B, N, N, N, M, N, N, B, N, B, M, N, N, N, N, B, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, M, N, N, N, N}
            ,{ N, N, B, N, N, N, B, B, B, B, B, N, B, N, M, N, B, B, N, N, N, N, N, B, N, N, N, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, B, N, N, N, B, B, B, N, N, N, N, N, M, N, N, N, N, N, N, N, B, N, N, N, N, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}
            ,{ N, N, B, N, N, N, B, B, N, B, B, N, B, N, M, N, N, B, B, B, N, N, N, N, N, B, B, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N, N}};



    public int getIndex() {
        return index;
    }

    public String getNom() {
        return nom;
    }

    public Groupe getGroupe() {
        return groupe;
    }

    public Saison getSaison() {
        return saison;
    }

    public Climat getClimat() {
        return climat;
    }

    // recupération de l'image après redimensionnement
    public ImageIcon getIcon() {
        try {
            BufferedImage bi = ImageIO.read(new File(icon));
            return new ImageIcon( bi.getScaledInstance(48, 48, Image.SCALE_SMOOTH) );
        } catch (IOException e) {
            System.out.println("\nERREUR:\n    Impossible de lire le fichier "+ icon);
            return null;
        }
    }

    public String getDescription() {
        return description;
    }

    // Nombre total de Plants : 47 pour l'instant
    public static int nbPlants() {
        return Plant.values().length;
    }

}
