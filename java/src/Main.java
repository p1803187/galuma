
import javax.swing.*;
import java.io.*;
import java.lang.ProcessBuilder;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    private static final boolean AFFICHER_MESSAGES_EXEC = true;

    public static final String TITRE = "GALUMA";
    public static final String LIEN = "https://forge.univ-lyon1.fr/p1803187/galuma";
    public static final String AUTEURS = "Gabriel JUSTIN, Lucas BARTHELEMY, Marine MASINGARBE";

    // METHODE MAIN
    public static void main (String[] args) {
        InterfaceGraphique interFaceG = new InterfaceGraphique();
        interFaceG.initialisationInterface();
    } // fin de Main()
    // Main utilisé lors de la première version
    public static void mainAffichageConsole()
    {
        sayln("#==== Hello from "+ TITRE+ ".Main");
        sayln("#==== Lien GitLab : "+ LIEN);
        sayln();
        sayln("#==============================================================");
        sayln();

        AffichageConsole interfaceProg = new AffichageConsole();
        int[][] tabParcelles = interfaceProg.affichage();
        int taillePot = countParcel(tabParcelles, interfaceProg.getHauteur(), interfaceProg.getLongueur());
        Potager curpotager = new Potager(taillePot);
        //curpotager.printAdj(tabParcelles,taillePot);

        curpotager.sauvegarderTabProx(tabParcelles,taillePot,new Plant[0]);
        try {
            ProcessBuilder pb = new ProcessBuilder("bash", "scriptPl");
            final Process process = pb.start();
            try{ process.waitFor(); }
            catch (Exception ex) {
                System.out.println("Un probleme lors de l'execution du prolog");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        File fileSolution = new File("../../prolog/solution.txt");
        int cptSol = 1;
        String[] sol;
        String[] sol2;
        String data;
        try {
            Scanner readSolution = new Scanner(fileSolution);
            while (readSolution.hasNextLine()) {
                data = readSolution.nextLine();
                sol = data.split("\\[");
                sol[1]=sol[1].substring(0,sol[1].length()-1);
                System.out.print("Solution "+cptSol+" : Score = "+sol[0]+" Legumes = ");
                sol2 = sol[1].split(",");
                for (String s : sol2) {
                    System.out.print(s + " ");
                }
                System.out.print("\n");
                ++cptSol;
            }
        }
        catch (FileNotFoundException exceptionNF) {
            System.out.println("Erreur lors de la lecture des solutions");
            exceptionNF.printStackTrace();
        }

        sayln();
        sayln("#==============================================================");
        sayln();
        sayln("#==== FIN du Programme");

    }


    // ===================================== FONCTIONS PRIVEES ===================================== //
    //méthode servant à compter le nombre de parcelles à partir d'un tableau à deux dimensions
    private static int countParcel(int[][] tabParcel, int hauteur, int longueur) {
        int compval, k, l;
        boolean doublon;
        int taille = hauteur*longueur;
        for (int i = 0; i < longueur; ++i) {
            for (int j = 0; j < hauteur; ++j) {
                compval = tabParcel[i][j];
                k = i;
                l = j;
                if (l+1 == hauteur) {
                    l = 0;
                    ++k;
                } else {
                    ++l;
                }
                doublon = false;
                while ((k < longueur) && !doublon) {
                    while ((l < hauteur) && !doublon) {
                        if (compval == tabParcel[k][l])
                            doublon = true;
                        ++l;
                    }
                    ++k;
                    l = 0;
                }
                if (doublon)
                    taille--;
            }
        }
        return taille;
    }



    // ======================================== UTILITAIRES ======================================== //

    // execute une commande dans la console
    public static void exec(final String COMMANDE) {
        try {
            Process process = Runtime.getRuntime().exec(COMMANDE);
            StringBuilder output = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line+ "\n");
            }
            int exitVal = process.waitFor();
            if (exitVal == 0) {
                if (AFFICHER_MESSAGES_EXEC) {
                    sayln(output.toString());
                }
            } else {
                sayln("AN ERROR OCCURRED :(");
                exit(1);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    // affiche un message dans le Terminal
    public static void say(final String MESSAGE) {
        System.out.print(MESSAGE);
    }

    // affiche un message dans le Terminal puis retourne a la ligne
    public static void sayln(final String MESSAGE) {
        System.out.println(MESSAGE);
    }

    // retourne a la ligne dans le Terminal
    public static void sayln() {
        System.out.println(" ");
    }

    // termine le programme
    public static void exit(final int VALUE) {
        exit(VALUE);
    }


}