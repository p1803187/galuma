
package util;

public class IntPair {

    public static final IntPair NULL = new IntPair(0, 0);

    private int left;
    private int right;

    public IntPair() {
        left = 0;
        right = 0;
    }

    public IntPair(final int LEFT, final int RIGHT) {
        left = LEFT;
        right = RIGHT;
    }

    public boolean Eq(final IntPair PAIR) {
        return ((left == PAIR.left)&&(right == PAIR.right) || (left == PAIR.right)&&(right == PAIR.left));
    }

    public void setLeft(final int VALUE) {
        left = VALUE;
    }

    public void setRight(final int VALUE) {
        right = VALUE;
    }

    public int getLeft() {
        return left;
    }

    public int getRight() {
        return right;
    }

}
