
package util;

import static java.lang.System.exit;

public class IntPairList {

    private int length;
    private IntPair[] array;

    public IntPairList() {
        array = new IntPair[0];
    }

    public void add(final IntPair PAIR) {
        extend(1);
        array[length-1] = PAIR;
    }

    /*public void add(final IntPair[] ARRAY) {
        extend(ARRAY.length);
        array[length-ARRAY.length-1] = PAIR;
    }*/

    private void extend(final int EXTRA_LENGTH) {
        IntPair[] result = new IntPair[length+EXTRA_LENGTH];
        for (int i = 0; i < length; i++) {
            result[i] = array[i];
        }
        array = result;
        length = array.length;
    }

    public boolean contains(final IntPair PAIR) {
        for (int i = 0; i < length; i++) {
            if (array[i].Eq(PAIR))
                return true;
        }
        return false;
    }

    public int getLength() {
        return length;
    }

    public IntPair getValue(final int I) {
        if (I >= length) {
            System.out.println("L'indice ("+ I+ ") est invalide.");
            exit(1);
        }
        return array[I];
    }

}
