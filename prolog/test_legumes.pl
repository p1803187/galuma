

/*
SoluceFile sert à générer les solutions. Il charge d'abord prox.pl puis écrit le score et la liste d'association pour chaque configuration valable.
*/

soluceFile:- 
[prox],
open('solution.txt',write,Stream),
scoremax3(S,Lass),
forall(member(X,Lass),writeSol(S,X,Stream)),
close(Stream).

/*
prédicat aidant l'écriture dans le fichier txt
*/
writeSol(S,Sol,Stream):-
write(Stream,S),
write(Stream,Sol),
write(Stream,"\n").

/*
génère la liste d'adjacence à partir des prédicats prox chargés dans prox.pl
*/

listadj(L):-
findall((X,Y),prox(X,Y),L).

/*
lasso crée une liste d'associations (indice,légume) de la forme [(1,tomate),(2,aubergine),(3,mais)...]
grâce au prédicat legume.
*/

lasso([],[]).

lasso(([(X,Y)|L]),[(X,Leg1),(Y,Leg2)|L2]):-
lasso(L,L2),
not(member((X,_),L2)),
not(member((Y,_),L2)),
legume(Leg1),
legume(Leg2).

lasso(([(X,Y)|L]),[(Y,Leg2)|L2]):-
lasso(L,L2),
member((X,_),L2),
not(member((Y,_),L2)),
legume(Leg2).

lasso(([(X,Y)|L]),[(X,Leg1)|L2]):-
lasso(L,L2),
not(member((X,_),L2)),
member((Y,_),L2),
legume(Leg1).

lasso(([(X,Y)|L]),L2):-
lasso(L,L2),
member((X,_),L2),
member((Y,_),L2).


/*
scorelist2 génère une solution en appelant lasso qui génère une liste d'associations et sumscore qui calcule le score pour cette configuration
*/

scorelist2(L,Lass,S):-
lasso(L,Lass),
sumscore(L,Lass,S).

/*
calcule le score à partir de la liste d'associations
*/
sumscore([],_,0).

sumscore([(X,Y)|L],Lass,S):-
sumscore(L,Lass,S3),
member((X,Leg1),Lass),
member((Y,Leg2),Lass),
synergie(Leg1,Leg2,S2),	
S is S2+S3.

/*
scoremax3 calcule le score maximal en créant une liste de tous les couples (listeAssociation, scoreAssocié) puis en appelant getMax qui calcule le score maximal de toutes
les configurations puis selectSol qui sélectionne toutes les solutions optimales.
*/

scoremax3(SM, Lres):-
listadj(L),
findall((Lass,SM),scorelist2(L,Lass,SM),Lsel),
getMax(Lsel,SM),
selectSol(SM,Lsel,Lres),!.

/*
getMax permet d'avoir le score maximal.
*/

getMax([(_,S)],S).

getMax([(_,S)|[X|Lres]],SM):-
getMax([X|Lres],S2),
SM is max(S,S2).

/*
parcourt la liste des solution et récupère celles qui sont les plus optimisées (score maximisé)
*/
selectSol(_,[],[]).

selectSol(SM,[(Ladj,S)|Lsel],[Ladj|Lres2]):-
S==SM,
selectSol(SM,Lsel,Lres2).

selectSol(SM,[(S,_)|Lsel],Lres2):-
S\==SM,
selectSol(SM,Lsel,Lres2).

/*
version alternative de scorelist adaptée à la modification de légumes
*/
scorelistbis(L,Lass,Lass2,S,V):-
lassobis(Lass,V,Lass2),
sumscore(L,Lass2,S).

/*
version alternative de lasso adaptée à la modification de légumes, on associe un légume parmi la liste des modifications avec legume2
*/
lassobis([(X,L)|Lass],V,[(X,L)|Lass2]):-
X\==V,
lassobis(Lass,V,Lass2).

lassobis([(V,L)|Lass],V,[(V,L2)|Lass]):-
legume2(L2).

/*
version alternative de scoremax3 adaptée à la modification de légumes
*/
scoremaxbis(SM,Lres,V,Lass):-
listadj(L),
findall((Lass2,S),scorelistbis(L,Lass,Lass2,S,V),Lsel),
getMax(Lsel,SM),
selectSol(SM,Lsel,Lres).
/*
version alternative de solucefile, charge le fichier prox2.pl
*/
soluceFile2(Lass,V):-
[prox],
[prox2],
open('solution.txt',write,Stream),
scoremaxbis(SM,Lres,V,Lass),
forall(member(X,Lres),writeSol(SM,X,Stream)),
close(Stream).